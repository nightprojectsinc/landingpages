<?php
$admin = 'Conek-1@ya.ru';

function notempty($str){
$str1= substr( $_POST[$str], 0, 64 );
$error = '';
if ( empty( $str1 ) ) $error = $error.'<li>Не заполнено поле '.$str.'</li>';
}

if ( isset( $_POST['sendMail'] ) ) {
  $surname  = substr( $_POST['surname'], 0, 64 );
  $name  = substr( $_POST['name'], 0, 64 );
  $midlename  = substr( $_POST['midlename'], 0, 64 );
  $email   = substr( $_POST['email'], 0, 64 );
  $olds = substr( $_POST['olds'], 0, 64 );
  $city  = substr( $_POST['city'], 0, 64 );
  $num  = substr( $_POST['num'], 0, 64 );
  
  $error = '';
  if ( empty( $surname ) ) $error = $error.'<tr><td colspan="2" style="color:red;">*Не заполнено поле "Фамилия"</td></tr>';
  if ( empty( $name ) ) $error = $error.'<tr><td colspan="2" style="color:red;">*Не заполнено поле "Имя"</td></tr>';
  if ( empty( $midlename ) ) $error = $error.'<tr><td colspan="2" style="color:red;">*Не заполнено поле "Отчество"</td></tr>';
  if ( empty( $olds ) ) $error = $error.'<tr><td colspan="2" style="color:red;">*Не заполнено поле "Дата рождения"</td></tr>';
  if ( empty( $city ) ) $error = $error.'<tr><td colspan="2" style="color:red;">*Не заполнено поле "Город"</td></tr>';
  if ( empty( $num ) ) $error = $error.'<tr><td colspan="2" style="color:red;">*Не заполнено поле "Телефон"</td></tr>';
  if ( empty( $email ) ) $error = $error.'<tr><td colspan="2" style="color:red;">*Не заполнено поле "E-mail"</td></tr>';
  if ( !empty( $email ) and !preg_match( "#^[0-9a-z_\-\.]+@[0-9a-z\-\.]+\.[a-z]{2,6}$#i", $email ) )
    $error = $error.'<tr><td colspan="2" style="color:red;">*поле "E-mail" должно соответствовать формату email@mail.ru</td></tr>';
  if ( !empty( $error ) ) {
    $_SESSION['sendMailForm']['error']   = '<tr><td colspan="2" style="color:red;">При заполнении формы были допущены ошибки:</td></tr>'.$error;
    $_SESSION['sendMailForm']['surname']    = $surname;
	$_SESSION['sendMailForm']['name']    = $name;
	$_SESSION['sendMailForm']['midlename']    = $midlename;
	$_SESSION['sendMailForm']['olds']    = $olds;
    $_SESSION['sendMailForm']['email']   = $email;
	$_SESSION['sendMailForm']['ind']    = $ind;
	$_SESSION['sendMailForm']['city']    = $city;
	$_SESSION['sendMailForm']['st']    = $st;
	$_SESSION['sendMailForm']['h']    = $h;
	$_SESSION['sendMailForm']['num']    = $num;
    header( 'Location: '.$_SERVER['PHP_SELF'] );
    die();
  }
  
  $body = "ФИО:\r\n".$surname." ".$name." ".$midlename."\r\n\r\n";
  $body .= "E-MAIL:\r\n".$email."\r\n\r\n";
  $body .= "Дата рождения:\r\n".$olds."\r\n\r\n";
  $body .= "Адрес:\r\n".$ind." ".$city." ".$st." ".$h."\r\n\r\n";
  $body .= "Телефон:\r\n".$num."\r\n\r\n";
  $body = quoted_printable_encode( $body );

  $theme   = '=?windows-1251?B?'.base64_encode('Заполнена форма на сайте').'?=';
  $headers = "From: ".$_SERVER['SERVER_NAME']." <".$email.">\r\n";
  $headers = $headers."Return-path: <".$email.">\r\n";
  $headers = $headers."Content-type: text/plain; charset=\"windows-1251\"\r\n";
  $headers = $headers."Content-Transfer-Encoding: quoted-printable\r\n\r\n";
  
  if ( mail($admin, $theme, $body, $headers) )
    $_SESSION['success'] = true;
  else
    $_SESSION['success'] = false;
  header( 'Location: '.$_SERVER['PHP_SELF'] );
  die();
}
 if (!function_exists('quoted_printable_encode')) {
function quoted_printable_encode ( $string ) {
   // rule #2, #3 (leaves space and tab characters in tact)
   $string = preg_replace_callback (
   '/[^\x21-\x3C\x3E-\x7E\x09\x20]/',
   'quoted_printable_encode_character',
   $string
   );
   $newline = "=\r\n"; // '=' + CRLF (rule #4)
   // make sure the splitting of lines does not interfere with escaped characters
   // (chunk_split fails here)
   $string = preg_replace ( '/(.{73}[^=]{0,3})/', '$1'.$newline, $string);
   return $string;
}}

function quoted_printable_encode_character ( $matches ) {
   $character = $matches[0];
   return sprintf ( '=%02x', ord ( $character ) );
}

if ( isset( $_SESSION['success'] ) ) {
  if ( $_SESSION['success'] )
    echo '<tr><td colspan="2"  style="color:green;">Письмо успешно отправлено</td></tr>';
  else
    echo '<tr><td colspan="2"  style="color:red;">Ошибка при отправке письма</td></tr>';
  unset( $_SESSION['success'] );
}
if ( isset( $_SESSION['sendMailForm'] ) ) {
  $surname    = htmlspecialchars ( $_SESSION['sendMailForm']['surname'] );
  $name    = htmlspecialchars ( $_SESSION['sendMailForm']['name'] );
  $midlename    = htmlspecialchars ( $_SESSION['sendMailForm']['midlename'] );
  $olds    = htmlspecialchars ( $_SESSION['sendMailForm']['olds'] );
  $email   = htmlspecialchars ( $_SESSION['sendMailForm']['email'] );
  $ind    = htmlspecialchars ( $_SESSION['sendMailForm']['ind'] );
  $city = htmlspecialchars ( $_SESSION['sendMailForm']['city'] );
  $st = htmlspecialchars ( $_SESSION['sendMailForm']['st'] );
  $h    = htmlspecialchars ( $_SESSION['sendMailForm']['h'] );
  $num    = htmlspecialchars ( $_SESSION['sendMailForm']['num'] );
  $call    = htmlspecialchars ( $_SESSION['sendMailForm']['call'] );
  
} else {
   $surname    = '';
  $name    = '';
  $midlename    = '';
  $olds    = '';
  $email   = '';
  $ind    = '';
  $city = '';
  $st = '';
  $h    = '';
  $num    = '';
  $call    = '';
}

if ( isset( $_SESSION['sendMailForm'] ) ) {
  echo $_SESSION['sendMailForm']['error'];
  unset( $_SESSION['sendMailForm'] );
}

?>
