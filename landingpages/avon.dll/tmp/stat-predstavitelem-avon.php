<?php
session_start();
header("Content-Type: text/html; charset=utf-8");
$admin = 'Conek-1@ya.ru';

function notempty($str){
$str1= substr( $_POST[$str], 0, 64 );
$error = '';
if ( empty( $str1 ) ) $error = $error.'<li>Не заполнено поле '.$str.'</li>';
}

if ( isset( $_POST['sendMail'] ) ) {
  $surname  = substr( $_POST['surname'], 0, 64 );
  $name  = substr( $_POST['name'], 0, 64 );
  $midlename  = substr( $_POST['midlename'], 0, 64 );
  $email   = substr( $_POST['email'], 0, 64 );
  $olds = substr( $_POST['olds'], 0, 64 );
  $city  = substr( $_POST['city'], 0, 64 );
  $num  = substr( $_POST['num'], 0, 64 );
  
  $error = '';
  if ( empty( $surname ) ) $error = $error.'<tr><td colspan="2" style="color:red;">*Не заполнено поле "Фамилия"</td></tr>';
  if ( empty( $name ) ) $error = $error.'<tr><td colspan="2" style="color:red;">*Не заполнено поле "Имя"</td></tr>';
  if ( empty( $midlename ) ) $error = $error.'<tr><td colspan="2" style="color:red;">*Не заполнено поле "Отчество"</td></tr>';
  if ( empty( $olds ) ) $error = $error.'<tr><td colspan="2" style="color:red;">*Не заполнено поле "Дата рождения"</td></tr>';
  if ( empty( $city ) ) $error = $error.'<tr><td colspan="2" style="color:red;">*Не заполнено поле "Город"</td></tr>';
  if ( empty( $num ) ) $error = $error.'<tr><td colspan="2" style="color:red;">*Не заполнено поле "Телефон"</td></tr>';
  if ( empty( $email ) ) $error = $error.'<tr><td colspan="2" style="color:red;">*Не заполнено поле "E-mail"</td></tr>';
  if ( !empty( $email ) and !preg_match( "#^[0-9a-z_\-\.]+@[0-9a-z\-\.]+\.[a-z]{2,6}$#i", $email ) )
    $error = $error.'<tr><td colspan="2" style="color:red;">*поле "E-mail" должно соответствовать формату email@mail.ru</td></tr>';
  if ( !empty( $error ) ) {
    $_SESSION['sendMailForm']['error']   = '<tr><td colspan="2" style="color:red;">При заполнении формы были допущены ошибки:</td></tr>'.$error;
    $_SESSION['sendMailForm']['surname']    = $surname;
	$_SESSION['sendMailForm']['name']    = $name;
	$_SESSION['sendMailForm']['midlename']    = $midlename;
	$_SESSION['sendMailForm']['olds']    = $olds;
    $_SESSION['sendMailForm']['email']   = $email;
	$_SESSION['sendMailForm']['ind']    = $ind;
	$_SESSION['sendMailForm']['city']    = $city;
	$_SESSION['sendMailForm']['st']    = $st;
	$_SESSION['sendMailForm']['h']    = $h;
	$_SESSION['sendMailForm']['num']    = $num;
    header( 'Location: '.$_SERVER['PHP_SELF'] );
    die();
  }
  
  $body = "ФИО:\r\n".$surname." ".$name." ".$midlename."\r\n\r\n";
  $body .= "E-MAIL:\r\n".$email."\r\n\r\n";
  $body .= "Дата рождения:\r\n".$olds."\r\n\r\n";
  $body .= "Адрес:\r\n".$ind." ".$city." ".$st." ".$h."\r\n\r\n";
  $body .= "Телефон:\r\n".$num."\r\n\r\n";
  $body = quoted_printable_encode( $body );

  $theme   = '=?windows-1251?B?'.base64_encode('Заполнена форма на сайте').'?=';
  $headers = "From: ".$_SERVER['SERVER_NAME']." <".$email.">\r\n";
  $headers = $headers."Return-path: <".$email.">\r\n";
  $headers = $headers."Content-type: text/plain; charset=\"windows-1251\"\r\n";
  $headers = $headers."Content-Transfer-Encoding: quoted-printable\r\n\r\n";
  
  if ( mail($admin, $theme, $body, $headers) )
    $_SESSION['success'] = true;
  else
    $_SESSION['success'] = false;
  header( 'Location: '.$_SERVER['PHP_SELF'] );
  die();
}
 if (!function_exists('quoted_printable_encode')) {
function quoted_printable_encode ( $string ) {
   // rule #2, #3 (leaves space and tab characters in tact)
   $string = preg_replace_callback (
   '/[^\x21-\x3C\x3E-\x7E\x09\x20]/',
   'quoted_printable_encode_character',
   $string
   );
   $newline = "=\r\n"; // '=' + CRLF (rule #4)
   // make sure the splitting of lines does not interfere with escaped characters
   // (chunk_split fails here)
   $string = preg_replace ( '/(.{73}[^=]{0,3})/', '$1'.$newline, $string);
   return $string;
}}

function quoted_printable_encode_character ( $matches ) {
   $character = $matches[0];
   return sprintf ( '=%02x', ord ( $character ) );
}
?>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
<title>Анкета представителя</title>
</head>
<?php include('header.php'); // Вставка шапки сайта ?>


<div id="main"> 
<div id="content">
<div id="questionnaire">
<h2>СТАНЬ ПРЕДСТАВИТЕЛЕМ AVON</h2>
<img src="image/nrdp11_300_790.jpg" alt="logo" style="float:right;">
<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
<table style="width:690px;">
   <tr> <td id="name" style=" padding-top:10px; " valign="bottom" colspan="2"> <p>С чего начать? Заполни анкету!</p>
<p>Стать Представителем Avon просто!</p>
<p><i>Шаг 1.</i> Заполните анкету, которая находится перед Вами. </p>
<p><i>Шаг 2.</i> Нажмите кнопку *Отправить*.</p>
<p>В ближайшее время с Вами свяжется Координатор Avon
и поможет оформить документы на регистрацию в качестве 
Представителя Avon.</p></td> </tr>
<?php
if ( isset( $_SESSION['success'] ) ) {
  if ( $_SESSION['success'] )
    echo '<tr><td colspan="2"  style="color:green;">Письмо успешно отправлено</td></tr>';
  else
    echo '<tr><td colspan="2"  style="color:red;">Ошибка при отправке письма</td></tr>';
  unset( $_SESSION['success'] );
}
if ( isset( $_SESSION['sendMailForm'] ) ) {
  $surname    = htmlspecialchars ( $_SESSION['sendMailForm']['surname'] );
  $name    = htmlspecialchars ( $_SESSION['sendMailForm']['name'] );
  $midlename    = htmlspecialchars ( $_SESSION['sendMailForm']['midlename'] );
  $olds    = htmlspecialchars ( $_SESSION['sendMailForm']['olds'] );
  $email   = htmlspecialchars ( $_SESSION['sendMailForm']['email'] );
  $ind    = htmlspecialchars ( $_SESSION['sendMailForm']['ind'] );
  $city = htmlspecialchars ( $_SESSION['sendMailForm']['city'] );
  $st = htmlspecialchars ( $_SESSION['sendMailForm']['st'] );
  $h    = htmlspecialchars ( $_SESSION['sendMailForm']['h'] );
  $num    = htmlspecialchars ( $_SESSION['sendMailForm']['num'] );
  $call    = htmlspecialchars ( $_SESSION['sendMailForm']['call'] );
  
} else {
   $surname    = '';
  $name    = '';
  $midlename    = '';
  $olds    = '';
  $email   = '';
  $ind    = '';
  $city = '';
  $st = '';
  $h    = '';
  $num    = '';
  $call    = '';
}
?>

   <tr> <td id="surname"> Фамилия*:</td>                   <td> <input type = "text" name = "surname"  value="<?php echo $surname ?>"></td> </tr>
   <tr> <td id="name"> Имя*:</td>                          <td> <input type = "text" name = "name"  value="<?php echo $name ?>"></td> </tr>
   <tr> <td id="midlename"> Отчество*:</td>                <td> <input type = "text" name = "midlename"  value="<?php echo $midlename ?>"></td> </tr>
   <tr> <td id="olds"> Дата рождения(дд/мм/гггг)*:</td>    <td> <input type = "text" name = "olds"  value="<?php echo $olds ?>"></td> </tr>
   <tr> <td id="email"> E-mail*:</td>                      <td> <input type = "text" name = "email"  value="<?php echo $email ?>"></td> </tr>
   <tr> <td id="ind"> Индекс:</td>                         <td> <input type = "text" name = "ind"  value="<?php echo $ind ?>"></td> </tr>
   <tr> <td id="city"> Город*:</td>                        <td> <input type = "text" name = "city"  value="<?php echo $city ?>"></td> </tr>
   <tr> <td id="st"> Улица:</td>                           <td> <input type = "text" name = "st"  value="<?php echo $st ?>"></td> </tr>
   <tr> <td id="h"> Дом/корпус/квартира:</td>              <td> <input type = "text" name = "h"  value="<?php echo $h ?>"></td> </tr>
   <tr> <td id="num"> Телефон*:</td>                       <td> <input type = "text" name = "num"  value="<?php echo $num ?>"></td> </tr>
   <tr> <td id="call"> Удобное время для звонка: </td> <td>
															<select name = "call">
															  <option value = "0">Выберите</option>														
															  <option value = "1">Утром</option>
															  <option value = "2">После обеда</option>
															  <option value = "3">Вечером</option>
    </select></td> </tr>
	 <tr><td colspan="2" style=" font-size: 10px;" >* - обязательные для заполения поля</td></tr>
    <tr><td colspan="2" style="text-align: center;" >        <input  style="width:120px; height:30px; font-size: 16px;" type = "submit" value = "Отправить" name="sendMail"></td></tr>
	
	<?php
if ( isset( $_SESSION['sendMailForm'] ) ) {
  echo $_SESSION['sendMailForm']['error'];
  unset( $_SESSION['sendMailForm'] );
}
?>
</table>
</form>
</div>
<div id="questionnaire" >
<h2 >В AVON КАЖДЫЙ МОЖЕТ ИСПОЛНИТЬ МЕЧТЫ,
ВЕДЬ МЫ ДЕЙСТВИТЕЛЬНО ПОНИМАЕМ, ЧТО ЛЮБЯТ ЖЕНЩИНЫ!</h2>
<img src="image/quest.jpg" style="width:877px; margin:20px 60px;" alt="logo">
<h3><i>ВОЗМОЖНОСТИ</i> И ПРИВИЛЕГИИ</h3>
<p>Мечтаешь самостоятельно планировать свой день и свой доход?</p>
<p>Решать, сколько времени посвятить семье и личной жизни, работе или учебе, любимому делу?</p>
<p>Начни свой собственный бизнес без финансовых затрат — стань Представителем Avon!</p>
<p>Заниматься любимым делом, получать дополнительный доход и все успевать вместе с Avon — это так просто!</p>

<h3><i>ТВОЕ УСПЕШНОЕ БУДУЩЕЕ</i> НАЧИНАЕТСЯ СЕГОДНЯ!</h3>

<p><i>Регистрируется</i> в Компании Avon бесплатно!</p>
<p><i>Получает</i> товары каталога AVON со скидкой до 31%!</p>
<p><i>Участвует</i> в специальных предложениях для Представителей компании Avon</p>
<p><i>Получает</i> продукцию на почте недалеко от дома</p>
<p><i>Получает</i> поддержку Координатора!</p>
<p><i>Первые шесть кампаний</i> участвует в Программе развития новых представителей – и получает каждую кампанию по подарку</p>
<p><i>Имеет возможность</i> размещать заказы через почту, интернет или SMS-сообщения</p>
<p><i>Имеет возможность</i> оплачивать заказы через пять дней после их получения на Почте, в Сбербанке или с помощью терминалов</p>
<p><i>Всегда в курсе</i> всех новинок, может заказать любые пробники и новинки следующего каталога</p>
<p><i>Может</i> заказывать продукцию для себя со скидкой! В AVON нет минимального размера заказа</p>

<h3><i>ВМЕСТЕ С AVON</i> ТВОЙ УСПЕХ ГАРАНТИРОВАН!</h3>
<p>Предлагать Клиентам продукцию Avon — по словам наших Представителей — «Это одно удовольствие!.</p>
<p>Компания Avon инвестирует в масштабные рекламные кампании по всему миру и сотрудничает с яркими звездами международного и российского масштаба.</p>
<p>Каждые три недели для Вас и Ваших Клиентов выходит новый Каталог.!</p>
<p>В каждом каталоге Avon представлено более 1 500 косметических товаров и аксессуаров.</p>
<p>Каждый Каталог Avon — это новая интригующая история и новые секреты обольщения.</p>

<h3><i>ТВОЙ ДОХОД.</i> ТВОЕ РАЗВИТИЕ.</h3>
<h3>ТВОЙ БИЗНЕС <i>БЕЗ ФИНАНСОВЫХ ЗАТРАТ.</i></h3>
<p>Бесплатный стартовый набор материалов</p>
<p>Беспроцентный кредит от Компании Avon</p>
<p>Гибкая система скидок на продукцию</p>
<p>Подарки новым Представителям по программе развития нового представителя</p>
<p>Программы поощрения для Представителей</p>
<p>Клуб привилегированных Представителей</p>
<p>Клубная карта</p>
<p>Свободный доступ к VIP сервису</p>
<p>Возможность размещать Интернет-заказы с помощью Интернет-Каталога</p>

<h3><i>ПОЧЕМУ БОЛЕЕ 6 МИЛЛИОНОВ ПРЕДСТАВИТЕЛЕЙ ВО ВСЕМ МИРЕ ВЫБРАЛИ AVON?</i></h3>
<p>От Москвы до Владивостока Avon Россия предлагает каждому Представителю уникальный сервис!</p>
<p>Присоединяйся к более 6 000 000 успешных женщин по всему миру и получай дополнительные привилегии!</p>

<h2>ВОЗЬМИ КУРС НА СТАБИЛЬНОСТЬ И РОСТ ДОХОДА</h2>
<a href="stat-koordinatorom-avon.php"><img src="image/stat_koodinat.jpg" style="width:877px; margin:20px 60px;" alt=""> </a>
</div>

</div>
</div>


<?php include('footer.php'); // Вставка подвала сайта ?>