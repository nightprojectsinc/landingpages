﻿<?php
	//session_start();
	include_once "Engine/tpl_module.php";
	include_once "Engine/core.php";

	$page = get_page("main");
	$header = get_page("header");
	$footer = get_page("footer");
	$content = get_page("contentdef");

//Проводим замену тегов на отельные части шаблона
	$page = str_replace("{HEADER}", $header, $page);
	$page = str_replace("{FOOTER}", $footer, $page);

	switch ($_GET['page'])
	{
		case 'about':
			$about = get_page("about");
			$page = str_replace("{AJAX}", $about, $page);
			$page = str_replace("{TITLE}", "О нас", $page);
			break;

		case 'catalog':
			$catalog = get_page("catalog");
			$cat = get_page("imgcatpage");
			$fullcat = get_page('catfull');
			#Первый каталог
			$newcatalog = $fnames = glob("catalogsdir/1/*.jpg");
			foreach ($fnames as $val) 
			{
				$cats .= str_replace("{NAME}", $val, $cat);
			}
			$fullcats = str_replace("{FIMG}", $cats, $fullcat);
			$fullcats = str_replace("{FCATALOGNAME}", "Новинка!", $fullcats);
			unset($cats);
			#Второй каталог
			$prevcatalog = $fnames = glob("catalogsdir/2/*.jpg");
			foreach ($fnames as $val) 
			{
				$cats .= str_replace("{NAME}", $val, $cat);
			}
			$fullcats = str_replace("{SIMG}", $cats, $fullcats);
			$fullcats = str_replace("{SCATALOGNAME}", "Предыдущий", $fullcats);




			/*for($i = 0; $i <= count($folds); $i++)
			{
				if(empty($folds[$i]))
				{
					break;
				}
				$fnames = glob($folds[$i]."/*.jpg");
				foreach ($fnames as $val) 
				{
					$cats .= str_replace("{NAME}", $val, $cat);
				}
				$fullcat1 = str_replace("{IMG}", $cats, $fullcat);
				$fullcat1 = str_replace("{CATALOGNAME}", $folds[$i], $fullcat1);
				$fullcats .= $fullcat1;
				unset($fnames);
				unset($cats);
				unset($fullcat1);
			}*/
			$catalog = str_replace("{CAT}", $fullcats, $catalog);
			$page = str_replace("{AJAX}", $catalog, $page);
			$page = str_replace("{TITLE}", "Каталоги", $page);
			break;

		case 'stat-predstavitelem-avon':
			$predstav = get_page("predstav");
			$page = str_replace("{AJAX}", $predstav, $page);
			include_once "Engine/prestavfun.php";
			$page = str_replace("{AJAX}", $koord, $page);
			$page = str_replace("{SURNAME}", $surname, $page);
			$page = str_replace("{NAME}", $name, $page);
			$page = str_replace("{MIDLENAME}", $midlename, $page);
			$page = str_replace("{OLDS}", $olds, $page);
			$page = str_replace("{EMAIL}", $email, $page);
			$page = str_replace("{KNUM}", $knum, $page);
			$page = str_replace("{IND}", $ind, $page);
			$page = str_replace("{CITY}", $city, $page);
			$page = str_replace("{ST}", $st, $page);
			$page = str_replace("{H}", $h, $page);
			$page = str_replace("{NUM}", $num, $page);
			$page = str_replace("{TITLE}", "Стать представителем", $page);
			break;

		case 'stat-koordinatorom-avon':
			$koord = get_page("koord");
			include_once "Engine/koordfun.php";
			$page = str_replace("{AJAX}", $koord, $page);
			$page = str_replace("{SURNAME}", $surname, $page);
			$page = str_replace("{NAME}", $name, $page);
			$page = str_replace("{MIDLENAME}", $midlename, $page);
			$page = str_replace("{OLDS}", $olds, $page);
			$page = str_replace("{EMAIL}", $email, $page);
			$page = str_replace("{KNUM}", $knum, $page);
			$page = str_replace("{IND}", $ind, $page);
			$page = str_replace("{CITY}", $city, $page);
			$page = str_replace("{ST}", $st, $page);
			$page = str_replace("{H}", $h, $page);
			$page = str_replace("{NUM}", $num, $page);
			$page = str_replace("{TITLE}", "Стать координатором", $page);
			break;

		case 'contact':
			$contact = get_page("contact");
			$page = str_replace("{AJAX}", $contact, $page);
			$page = str_replace("{TITLE}", "Наши контакты", $page);
			break;

		
		default:
			$page = str_replace("{AJAX}", $content, $page);
			$page = str_replace("{TITLE}", "Главная", $page);
			break;
	}


	

	$page = str_replace("{TIMEEND}", timeend(), $page);

	print $page;



?>

