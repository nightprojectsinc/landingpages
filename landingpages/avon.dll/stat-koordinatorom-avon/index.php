<?php
session_start();
header("Content-Type: text/html; charset=utf-8");
$admin = 'Conek-1@ya.ru';

function notempty($str){
$str1= substr( $_POST[$str], 0, 64 );
$error = '';
if ( empty( $str1 ) ) $error = $error.'<li>Не заполнено поле '.$str.'</li>';
}

if ( isset( $_POST['sendMail'] ) ) {
  $surname  = substr( $_POST['surname'], 0, 64 );
  $name  = substr( $_POST['name'], 0, 64 );
  $midlename  = substr( $_POST['midlename'], 0, 64 );
  $email   = substr( $_POST['email'], 0, 64 );
  $olds = substr( $_POST['olds'], 0, 64 );
  $city  = substr( $_POST['city'], 0, 64 );
  $num  = substr( $_POST['num'], 0, 64 );
  
  $error = '';
  if ( empty( $surname ) ) $error = $error.'<tr><td colspan="2" style="color:red;">*Не заполнено поле "Фамилия"</td></tr>';
  if ( empty( $name ) ) $error = $error.'<tr><td colspan="2" style="color:red;">*Не заполнено поле "Имя"</td></tr>';
  if ( empty( $midlename ) ) $error = $error.'<tr><td colspan="2" style="color:red;">*Не заполнено поле "Отчество"</td></tr>';
  if ( empty( $olds ) ) $error = $error.'<tr><td colspan="2" style="color:red;">*Не заполнено поле "Дата рождения"</td></tr>';
  if ( empty( $city ) ) $error = $error.'<tr><td colspan="2" style="color:red;">*Не заполнено поле "Город"</td></tr>';
  if ( empty( $num ) ) $error = $error.'<tr><td colspan="2" style="color:red;">*Не заполнено поле "Телефон"</td></tr>';
  if ( empty( $email ) ) $error = $error.'<tr><td colspan="2" style="color:red;">*Не заполнено поле "E-mail"</td></tr>';
  if ( !empty( $email ) and !preg_match( "#^[0-9a-z_\-\.]+@[0-9a-z\-\.]+\.[a-z]{2,6}$#i", $email ) )
    $error = $error.'<tr><td colspan="2" style="color:red;">*поле "E-mail" должно соответствовать формату email@mail.ru</td></tr>';
  if ( !empty( $error ) ) {
    $_SESSION['sendMailForm']['error']   = '<tr><td colspan="2" style="color:red;">При заполнении формы были допущены ошибки:</td></tr>'.$error;
    $_SESSION['sendMailForm']['surname']    = $surname;
	$_SESSION['sendMailForm']['name']    = $name;
	$_SESSION['sendMailForm']['midlename']    = $midlename;
	$_SESSION['sendMailForm']['olds']    = $olds;
    $_SESSION['sendMailForm']['email']   = $email;
    $_SESSION['sendMailForm']['knum']   = $knum;
	$_SESSION['sendMailForm']['ind']    = $ind;
	$_SESSION['sendMailForm']['city']    = $city;
	$_SESSION['sendMailForm']['st']    = $st;
	$_SESSION['sendMailForm']['h']    = $h;
	$_SESSION['sendMailForm']['num']    = $num;
	$_SESSION['sendMailForm']['call']    = $call;
    header( 'Location: '.$_SERVER['PHP_SELF'] );
    die();
  }
  
  $body = "ФИО:\r\n".$surname." ".$name." ".$midlename."\r\n\r\n";
  $body .= "E-MAIL:\r\n".$email."\r\n\r\n";
  $body .= "Дата рождения:\r\n".$olds."\r\n\r\n";
  $body .= "Адрес:\r\n".$ind." ".$city." ".$st." ".$h."\r\n\r\n";
  $body .= "Телефон:\r\n".$num."\r\n\r\n";
  $body .= "Компьютерный нормер:\r\n".$knum."\r\n\r\n";
  $body = quoted_printable_encode( $body );

  $theme   = '=?windows-1251?B?'.base64_encode('Заполнена форма на сайте').'?=';
  $headers = "From: ".$_SERVER['SERVER_NAME']." <".$email.">\r\n";
  $headers = $headers."Return-path: <".$email.">\r\n";
  $headers = $headers."Content-type: text/plain; charset=\"windows-1251\"\r\n";
  $headers = $headers."Content-Transfer-Encoding: quoted-printable\r\n\r\n";
  
  if ( mail($admin, $theme, $body, $headers) )
    $_SESSION['success'] = true;
  else
    $_SESSION['success'] = false;
  header( 'Location: '.$_SERVER['PHP_SELF'] );
  die();
}
 if (!function_exists('quoted_printable_encode')) {
function quoted_printable_encode ( $string ) {
   // rule #2, #3 (leaves space and tab characters in tact)
   $string = preg_replace_callback (
   '/[^\x21-\x3C\x3E-\x7E\x09\x20]/',
   'quoted_printable_encode_character',
   $string
   );
   $newline = "=\r\n"; // '=' + CRLF (rule #4)
   // make sure the splitting of lines does not interfere with escaped characters
   // (chunk_split fails here)
   $string = preg_replace ( '/(.{73}[^=]{0,3})/', '$1'.$newline, $string);
   return $string;
}}

function quoted_printable_encode_character ( $matches ) {
   $character = $matches[0];
   return sprintf ( '=%02x', ord ( $character ) );
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="/style.css">
<title>Анкета координатора</title>
</head>
<?php include('../header.php'); // Вставка шапки сайта ?>

<div id="main"> 
<div id="content">
<div id="questionnaire">
<h2>СТАНЬ КООРДИНАТРОМ AVON</h2>
<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
<table style="width:580px; float:left; padding:40px 40px; ">
   <tr> <td id="name" style=" padding-top:10px; " valign="bottom" colspan="2">
<p><i>Шаг 1.</i> Заполните анкету, которая находится перед Вами. </p>
<p><i>Шаг 2.</i> Нажмите кнопку *Отправить*.</p>
<p>Чтобы стать Координатором заполните анкету
или позвоните прямо сейчас вашему Координатору
или Менеджеру и назначьте встречу!</p></td> </tr>

<?php
if ( isset( $_SESSION['success'] ) ) {
  if ( $_SESSION['success'] )
    echo '<tr><td colspan="2"  style="color:green;">Письмо успешно отправлено</td></tr>';
  else
    echo '<tr><td colspan="2"  style="color:red;">Ошибка при отправке письма</td></tr>';
  unset( $_SESSION['success'] );
}
if ( isset( $_SESSION['sendMailForm'] ) ) {
  $surname    = htmlspecialchars ( $_SESSION['sendMailForm']['surname'] );
  $name    = htmlspecialchars ( $_SESSION['sendMailForm']['name'] );
  $midlename    = htmlspecialchars ( $_SESSION['sendMailForm']['midlename'] );
  $olds    = htmlspecialchars ( $_SESSION['sendMailForm']['olds'] );
  $email   = htmlspecialchars ( $_SESSION['sendMailForm']['email'] );
  $knum   = htmlspecialchars ( $_SESSION['sendMailForm']['knum'] );
  $ind    = htmlspecialchars ( $_SESSION['sendMailForm']['ind'] );
  $city = htmlspecialchars ( $_SESSION['sendMailForm']['city'] );
  $st = htmlspecialchars ( $_SESSION['sendMailForm']['st'] );
  $h    = htmlspecialchars ( $_SESSION['sendMailForm']['h'] );
  $num    = htmlspecialchars ( $_SESSION['sendMailForm']['num'] );
  
} else {
   $surname    = '';
  $name    = '';
  $midlename    = '';
  $olds    = '';
  $email   = '';
  $knum   = '';
  $ind    = '';
  $city = '';
  $st = '';
  $h    = '';
  $num    = '';
}
?>
   <tr> <td id="surname"> Фамилия*:</td>                   <td> <input type = "text" name = "surname"  value="<?php echo $surname ?>"></td> </tr>
   <tr> <td id="name"> Имя*:</td>                          <td> <input type = "text" name = "name"  value="<?php echo $name ?>"></td> </tr>
   <tr> <td id="midlename"> Отчество*:</td>                <td> <input type = "text" name = "midlename"  value="<?php echo $midlename ?>"></td> </tr>
   <tr> <td id="olds"> Дата рождения(дд/мм/гггг)*:</td>    <td> <input type = "text" name = "olds"  value="<?php echo $olds ?>"></td> </tr>
   <tr> <td id="email"> E-mail*:</td>                      <td> <input type = "text" name = "email"  value="<?php echo $email ?>"></td> </tr>
   <tr> <td id="knum"> Компьютерный номер(Только для представителей):</td><td> <input type = "text" name = "knum"  value="<?php echo $knum ?>"></td> </tr>
   <tr> <td id="ind"> Индекс:</td>                         <td> <input type = "text" name = "ind"  value="<?php echo $ind ?>"></td> </tr>
   <tr> <td id="city"> Город*:</td>                        <td> <input type = "text" name = "city"  value="<?php echo $city ?>"></td> </tr>
   <tr> <td id="st"> Улица:</td>                           <td> <input type = "text" name = "st"  value="<?php echo $st ?>"></td> </tr>
   <tr> <td id="h"> Дом/корпус/квартира:</td>              <td> <input type = "text" name = "h"  value="<?php echo $h ?>"></td> </tr>
   <tr> <td id="num"> Телефон*:</td>                       <td> <input type = "text" name = "num"  value="<?php echo $num ?>"></td> </tr>
   <tr> <td id="call"> Удобное время для звонка: </td> <td>
															<select name = "call">
															  <option value = "0">Выберите</option>														
															  <option value = "1">Утром</option>
															  <option value = "2">После обеда</option>
															  <option value = "3">Вечером</option>
    </select></td> </tr>
	 <tr><td colspan="2" style=" font-size: 10px;" >* - обязательные для заполения поля</td></tr>
    <tr><td colspan="2" style="text-align: center;" ><input  style="width:120px; height:30px; font-size: 16px;" type = "submit" value = "Отправить" name="sendMail"></td></tr>
<?php
if ( isset( $_SESSION['sendMailForm'] ) ) {
  echo $_SESSION['sendMailForm']['error'];
  unset( $_SESSION['sendMailForm'] );
}
?>
</table>
</form>
</div>
<div style=" margin-left:600px; margin-bottom:20px;">
<img src="/image/new_coordinator_page_17_2.jpg" style="width:400px;" alt="logo">
<div style="width:385px; float:left; font-family: 'Times New Roman', Times, serif; font-size: 24px; font-style: italic; border-top:3px solid #e3008d; border-bottom:3px solid #e3008d; text-align: center;">
Чтобы стать Координатором заполните анкету
или позвоните прямо сейчас вашему Координатору
или Менеджеру и назначьте встречу!
</div>
<h2 style="font-weight:normal; text-align: center; margin-top:20px; font-size: 27px; font-style: italic; color:#e3008d;">Возможности AVON открыты для каждого</h2>
<div style="background: no-repeat url(/image/31_dohod.jpg); width:385px; height:77px; border-bottom:1px dashed #D2D0D0;">
<p style="position:relative; top:30px; left:80px;">До 31% дохода от личных продаж</p>
</div>
<div style="background: no-repeat url(/image/12_dohod.jpg); width:385px; height:77px; border-bottom:1px dashed #D2D0D0;">
<p style="position:relative; top:30px; left:80px;">До 12% дохода от продаж твоей команды</p>
</div>
<div style="background: no-repeat url(/image/bonusy.jpg); width:385px; height:77px; border-bottom:1px dashed #D2D0D0;">
<p style="position:relative; top:30px; left:80px;">Бонусы для Новых Координаторов</p>
</div>
<div style="background: no-repeat url(/image/program.jpg); width:385px; height:77px;">
<p style="position:relative; top:30px; left:80px;">Программы поощрения для Координаторов</p>
</div>
</div>


<div style="margin-top:110px;">
<h2 style="font-weight:normal; text-align: center; background-color: #ed008c; padding: 3px 20px 5px 20px; color: white; font-style: italic; font-size: 24px; width:900px; margin:0px auto;">3 простых шага обязательно приведут Вас к успеху!</h2> 
<h2 style="font-weight:normal; margin-top:20px; font-size: 27px; font-style: italic; color:#e3008d;">Координатор AVON:</h2>
<div style="background: no-repeat url(/image/1helps.jpg); width:900px; height:91px; border-bottom:1px dashed #D2D0D0;">
<p style="position:relative; top:30px; left:95px; font-style: italic;">помогает своим Клиентам правильно подобрать необходимые продукты из Каталога и делится опытом использования продукта Avon!</p>
</div>
<div style="background: no-repeat url(/image/2tells.jpg); width:900px; height:91px; border-bottom:1px dashed #D2D0D0;">
<p style="position:relative; top:30px; left:95px; font-style: italic;">вдохновляет своих Представителей личным примером, развивает успешных Координаторов, поддерживая их на пути к достижению своих целей, ведет свою команду за собой к новым достижениям и победам вместе с Avon!</p>
</div>
<div style="background: no-repeat url(/image/3inspires.jpg); width:900px; height:91px; border-bottom:1px dashed #D2D0D0;">
<p style="position:relative; top:30px; left:95px; font-style: italic;">рассказывает всем, как просто присоединиться к дружной команде Avon и создать свою успешную команду Представителей</p>
</div>
</div>
</div>


</div>
</div>


<?php include('../footer.php'); // Вставка подвала сайта ?>