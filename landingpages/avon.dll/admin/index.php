<?php
	session_start();
	include_once "engine/core.php";
	include_once "engine/tpl_module.php";
	include_once "engine/auth.php";
	include_once "engine/imglink.php";


	define("FPATH", "/admin");
	$page = get_page("main");
	$head = get_page("head");
	$imglink = get_page("imglink");

	$link = connect();
	$result = mysqli_query($link, "SELECT `username`, `name` FROM `users` WHERE 1");
	$result = mysqli_fetch_array($result, MYSQLI_ASSOC);
	if($_SESSION['login'] == $result['username'] && $_SESSION['name'] == $result['name'] && $_SESSION['status'] == "ready")
	{
		$afterauth = get_page("afterauth");
		$page = str_replace("{AJAX}", $afterauth, $page);
		$page = str_replace("{NAME}", $_SESSION['name'], $page);
		$page = str_replace("{IMGLINK}", $imglink, $page);
		$page = str_replace("{FPATH}", FPATH, $page);
		$head = str_replace("{TITLE}", "Панель управления", $head);
		$page = str_replace("{HEAD}", "Но пока что ее нет :с", $page);
		// Get images link
		

	}
	else
	{
		$auth = get_page("auth");
		$page = str_replace("{AJAX}", $auth, $page);
		$head = str_replace("{TITLE}", "Авторизация", $head);
		$page = str_replace("{HEAD}", "Тут возможно будет шапка :D", $page);
	}

	if(isset($_GET['exit']))
	{
		unset($_SESSION['login']);
		unset($_SESSION['name']);
		unset($_SESSION['status']);
		print("<script>location.reload();history.replaceState(null, null, '/admin');</script>");
	}

	print($head);
	print($page);

?>