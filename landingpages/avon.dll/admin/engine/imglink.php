<?php 

	function copyimg($link, $num, $dest)
	{
		$int = 0;
		while($int <= $num)
		{
			if($int < 10)$ch = "00".$int;
			if($int > 10 && $int < 100)$ch = "0".$int;
			if($int > 100)$ch = $int;
			copy($link."/p".$ch.".jpg", $dest."/p".$ch.".jpg");
			$int++;
		}
	}

	if($_POST['getimglink'] == "copy")
	{
		$link = $_POST['imglinkaddr'];
		$fold = $_POST['fold'];
		$num = $_POST['numberimgs'];
		if(!empty($link) && !empty($fold) && $num > 0)
		{
			if(file_exists("../catalogs/".$fold))
			{
				copyimg($link, $num, "../catalogs/".$fold);
			}
			else
			{
				mkdir("../catalogs/".$fold);
				copyimg($link, $num, "../catalogs/".$fold);
			}

		}
		else
			print("Вы что то пропустили :c");
	}
?>